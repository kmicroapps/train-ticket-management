const express = require('express');
const router = express.Router();
const reservationController = require('../controllers/reservation.controller');

router.post('/', reservationController.createReservation);
router.get('/', reservationController.getReservations);
router.get('/:id', reservationController.getReservationById);
router.put('/:id', reservationController.updateReservation);
router.patch('/:reservationId/payment', reservationController.completePayment);
router.delete('/:id', reservationController.deleteReservation);

module.exports = router;
