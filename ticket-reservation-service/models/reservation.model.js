const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReservationSchema = new Schema({
	ticketIds: [{ type: Schema.Types.ObjectId, ref: 'Ticket', required: true }],
	userId: { type: Schema.Types.ObjectId, ref: 'User', required: true },
	reservationDate: { type: Date, default: Date.now },
	paymentStatus: {
		type: String,
		enum: ['pending', 'completed', 'failed'],
		default: 'pending',
	},
});

module.exports = mongoose.model('Reservation', ReservationSchema);
