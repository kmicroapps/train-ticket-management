const Reservation = require('../models/reservation.model');
const axios = require('axios');

exports.createReservation = async (req, res) => {
	const { ticketIds, userId } = req.body;
	try {
		const newReservation = new Reservation({ ticketIds, userId });
		for (const ticketId of ticketIds) {
			const updateTicketResponse = await axios.patch(
				`${process.env.API_GATEWAY_URI}/ticket/${ticketId}/booking`,
				{
					status: 'reserved',
				}
			);

			if (updateTicketResponse.status !== 200) {
				throw new Error('Failed to update ticket status');
			}
		}

		const reservation = await newReservation.save();
		res.status(201).json(reservation);
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

exports.getReservations = async (req, res) => {
	try {
		const reservations = await Reservation.find()
			.populate('ticketIds')
			.populate('userId');
		res.status(200).json(reservations);
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

exports.getReservationById = async (req, res) => {
	try {
		const reservation = await Reservation.findById(req.params.id);
		if (!reservation) {
			return res.status(404).json({ message: 'Reservation not found' });
		}
		res.status(200).json(reservation);
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

exports.updateReservation = async (req, res) => {
	try {
		const reservation = await Reservation.findByIdAndUpdate(
			req.params.id,
			req.body,
			{ new: true }
		);
		if (!reservation) {
			return res.status(404).json({ message: 'Reservation not found' });
		}
		res.status(200).json(reservation);
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

exports.deleteReservation = async (req, res) => {
	try {
		const reservation = await Reservation.findByIdAndDelete(req.params.id);
		if (!reservation) {
			return res.status(404).json({ message: 'Reservation not found' });
		}
		res.status(200).json({ message: 'Reservation deleted' });
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

exports.completePayment = async (req, res) => {
	try {
		const { reservationId } = req.params;
		const reservation = await Reservation.findById(reservationId);

		if (!reservation) {
			return res.status(404).json({ message: 'Reservation not found' });
		}

		reservation.paymentStatus = 'completed';

		await reservation.save();

		res.status(200).json({
			message: 'Payment completed successfully',
			reservation,
		});
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};
