const express = require('express');
const router = express.Router();
const ticketController = require('../controllers/ticket.controller');

router.post('/', ticketController.createTicket);
router.get('/', ticketController.getTickets);
router.get('/detail/:id', ticketController.getTicket);
router.get('/:scheduleId', ticketController.getTicketsBySchedule);
router.patch('/:ticketId/booking', ticketController.updateTicketStatus);
router.put('/:id', ticketController.updateTicket);
router.delete('/:id', ticketController.deleteTicket);

module.exports = router;
