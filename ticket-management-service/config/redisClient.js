const redis = require('redis');
const { promisify } = require('util');

const client = redis.createClient({
	host: 'redis-18789.c1.ap-southeast-1-1.ec2.redns.redis-cloud.com',
	port: 18789,
});

client.ping(function (err, result) {
	console.log(result);
});

client.on('error', (err) => {
	console.error('Error connecting to Redis', err);
});

client.on('connect', () => {
	console.log('Connected to Redis');
});

const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const delAsync = promisify(client.del).bind(client);

module.exports = {
	client,
	getAsync,
	setAsync,
	delAsync,
};
