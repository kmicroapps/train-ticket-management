const Ticket = require('../models/ticket.model');

exports.createTicket = async (req, res) => {
	try {
		const { scheduleId, price, seat, status } = req.body;
		const ticket = new Ticket({ scheduleId, price, seat, status });
		await ticket.save();
		res.status(201).json({
			message: 'Ticket created successfully',
			ticket,
		});
	} catch (err) {
		res.status(500).json({ error: err.message });
	}
};

exports.getTickets = async (req, res) => {
	try {
		const tickets = await Ticket.find();
		res.status(200).json(tickets);
	} catch (err) {
		res.status(500).json({ error: err.message });
	}
};

exports.getTicket = async (req, res) => {
	try {
		const ticket = await Ticket.findById(req.params.id);
		if (!ticket) {
			return res.status(404).json({ message: 'Ticket not found' });
		}
		res.status(200).json(ticket);
	} catch (err) {
		res.status(500).json({ error: err.message });
	}
};

exports.updateTicket = async (req, res) => {
	try {
		const { scheduleId, price, seat, status } = req.body;
		const ticket = await Ticket.findByIdAndUpdate(
			req.params.id,
			{ scheduleId, price, seat, status },
			{ new: true }
		).populate('scheduleId');
		if (!ticket) {
			return res.status(404).json({ message: 'Ticket not found' });
		}
		res.status(200).json({
			message: 'Ticket updated successfully',
			ticket,
		});
	} catch (err) {
		res.status(500).json({ error: err.message });
	}
};

exports.deleteTicket = async (req, res) => {
	try {
		const ticket = await Ticket.findByIdAndDelete(req.params.id);
		if (!ticket) {
			return res.status(404).json({ message: 'Ticket not found' });
		}
		res.status(200).json({ message: 'Ticket deleted successfully' });
	} catch (err) {
		res.status(500).json({ error: err.message });
	}
};

exports.getTicketsBySchedule = async (req, res) => {
	try {
		const { scheduleId } = req.params;
		const tickets = await Ticket.find({ scheduleId });
		res.status(200).json(tickets);
	} catch (err) {
		res.status(500).json({ error: err.message });
	}
};

exports.updateTicketStatus = async (req, res) => {
	try {
		const { ticketId } = req.params;
		const { status } = req.body;

		// Kiểm tra trạng thái mới có hợp lệ không
		if (!['available', 'reserved', 'sold'].includes(status)) {
			return res.status(400).json({ message: 'Invalid status' });
		}

		// Tìm vé cần cập nhật trạng thái
		const ticket = await Ticket.findById(ticketId);
		if (!ticket) {
			return res.status(404).json({ message: 'Ticket not found' });
		}

		// Cập nhật trạng thái của vé
		ticket.status = status;
		await ticket.save();

		res.status(200).json({
			message: 'Ticket status updated successfully',
			ticket,
		});
	} catch (err) {
		res.status(500).json({ error: err.message });
	}
};
