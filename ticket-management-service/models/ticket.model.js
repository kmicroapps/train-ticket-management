const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TicketSchema = new Schema({
	scheduleId: {
		type: Schema.Types.ObjectId,
		ref: 'Schedule',
		required: true,
	},
	price: { type: Number, required: true },
	seat: { type: String, required: true },
	status: {
		type: String,
		enum: ['available', 'reserved', 'sold'],
		default: 'available',
	},

	createdAt: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Ticket', TicketSchema);
