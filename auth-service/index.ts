import express from "express";
import bodyParser from "body-parser";
import morgan from "morgan";
import cors from "cors";
import { PrismaClient } from "@prisma/client";

import config from "./configs/environment";
import { errorHandler } from "./middlewares/errorHandler";
import { authenticationMiddleware } from "./middlewares/authentication.middleware";

import { login, register, verifyToken } from "./controllers/auth";
import { getMe } from "./controllers/user";

const app = express();
const prisma = new PrismaClient();

app.use(morgan("dev"));
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
  req.context = {
    session: null,
    prisma,
  };

  next();
});

app.use("^/$", (req, res) => {
  return res.status(404).end();
});

app.post("/register", register);
app.post("/login", login);
app.get("/verifyToken", verifyToken);

app.get("/users/me", authenticationMiddleware, getMe);

app.use(errorHandler);

app.listen(config.PORT, () =>
  console.log(`server is running on: http://${config.HOST}:${config.PORT}`)
);
