import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";

import config from "../configs/environment";

import { UnAuthorizeError } from "../types/errors";

const regex = /Bearer (.+)/i;

const authenticationMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const token = req.headers["authorization"]?.match(regex)?.[1] || "";

    if (!token) throw new UnAuthorizeError();

    const verified = jwt.verify(token, config.JWT_SECRET_KEY);

    if (!verified) throw new UnAuthorizeError();

    req.context.session = verified;
    next();
  } catch (error: any) {
    next(error);
  }
};

export { authenticationMiddleware };
