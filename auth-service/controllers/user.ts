import { NextFunction, Request, Response } from "express";

export const getMe = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { session, prisma } = req.context;

  try {
    const userById = await prisma.user.findUnique({
      where: {
        id: session.userId,
      },
      select: {
        id: true,
        firstName: true,
        lastName: true,
        dateOfBirth: true,
        phoneNumber: true,
      },
    });

    return res.status(200).json(userById);
  } catch (error: any) {
    next(error);
  }
};
