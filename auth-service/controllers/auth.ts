import { NextFunction, Request, Response } from "express";
import { compareSync, hashSync } from "bcrypt";
import jwt from "jsonwebtoken";

import config from "../configs/environment";

import { LoginRequestParam, RegisterRequestParam } from "../types";
import { BadRequestError, ForbiddenError } from "../types/errors";

const emailRegex = new RegExp(
  /^[A-Za-z0-9_!#$%&'*+\/=?`{|}~^.-]+@[A-Za-z0-9.-]+$/,
  "gm"
);

export const register = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { prisma } = req.context;
  const registerParams: RegisterRequestParam = req.body;

  const saltRound = 10;

  try {
    if (
      !registerParams.email?.trim() ||
      !registerParams.password?.trim() ||
      !registerParams.firstName?.trim() ||
      !registerParams.lastName?.trim() ||
      !registerParams.phoneNumber?.trim()
    )
      throw new BadRequestError("invalid credentials");

    if (!emailRegex.test(registerParams.email))
      throw new ForbiddenError("invalid email");

    const emailExist = await prisma.account.count({
      where: {
        email: registerParams.email,
      },
    });

    if (emailExist) throw new ForbiddenError("email already in use");

    const userCreated = await prisma.user.create({
      data: {
        firstName: registerParams.firstName,
        lastName: registerParams.lastName,
        phoneNumber: registerParams.phoneNumber,
      },
      select: {
        id: true,
      },
    });

    const passwordHashed = hashSync(registerParams.password, saltRound);

    await prisma.account.create({
      data: {
        email: registerParams.email,
        password: passwordHashed,
        userId: userCreated.id,
      },
    });

    return res.status(201).end();
  } catch (error) {
    console.log("12345678: ", error);
    
    next(error);
  }
};

export const login = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { prisma } = req.context;
  const loginParams: LoginRequestParam = req.body;

  try {
    if (!loginParams.email?.trim() || !loginParams.password?.trim())
      throw new BadRequestError("invalid credentials");

    if (!emailRegex.test(loginParams.email))
      throw new ForbiddenError("invalid email");

    const accountByEmail = await prisma.account.findUnique({
      where: {
        email: loginParams.email,
      },
      select: {
        id: true,
        userId: true,
        email: true,
        password: true,
        role: true,
      },
    });

    if (!accountByEmail) throw new BadRequestError("account not exist");

    const passwordCompared = compareSync(
      loginParams.password,
      accountByEmail.password
    );

    if (!passwordCompared) throw new ForbiddenError("wrong password");

    const token = jwt.sign(
      {
        id: accountByEmail.id,
        userId: accountByEmail.userId,
        role: accountByEmail.role,
      },
      config["JWT_SECRET_KEY"],
      { algorithm: "HS256" }
    );

    return res.status(200).json({ token });
  } catch (error) {
    next(error);
  }
};

export const verifyToken = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { token = "" } = req.query;
  const tokenString = token.toString();

  try {
    if (!tokenString.trim()) throw new BadRequestError();

    const decoded = jwt.verify(tokenString, config.JWT_SECRET_KEY);

    return res.status(200).json(decoded);
  } catch (error: any) {
    next(error);
  }
};
