import { Account, PrismaClient } from "@prisma/client";

export {};

export interface Context {
  session: any;
  prisma: PrismaClient;
}

// ==============================================

export interface RegisterRequestParam {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  dateOfBirth?: Date;
  phoneNumber: string;
  address?: string;
}

export interface LoginRequestParam {
  email: string;
  password: string;
}
