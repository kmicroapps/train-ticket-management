export class BadRequestError extends Error {
  statusCode: number = 400;

  constructor(public message: string = "BAD_REQUEST") {
    super(message);
    Object.setPrototypeOf(this, BadRequestError.prototype);
  }
}

export class UnAuthorizeError extends Error {
  statusCode: number = 401;

  constructor(public message: string = "Unauthorized") {
    super(message);
    Object.setPrototypeOf(this, BadRequestError.prototype);
  }
}

export class ForbiddenError extends Error {
  statusCode: number = 403;

  constructor(public message: string = "Forbidden") {
    super(message);
    Object.setPrototypeOf(this, BadRequestError.prototype);
  }
}
