const express = require('express');
const router = express.Router();
const scheduleController = require('../controllers/schedule.controller');

// Create a new schedule
router.post('/', scheduleController.createSchedule);

// Get all schedules
router.get('/', scheduleController.getSchedules);

// Get a schedule by ID
router.get('/:id', scheduleController.getScheduleById);

// Update a schedule
router.put('/:id', scheduleController.updateSchedule);

// Delete a schedule
router.delete('/:id', scheduleController.deleteSchedule);

module.exports = router;
