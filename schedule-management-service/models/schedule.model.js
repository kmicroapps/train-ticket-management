const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ScheduleSchema = new Schema({
	event: { type: String, required: true },
	date: { type: Date, required: true },
	venue: { type: String, required: true },
	createdAt: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Schedule', ScheduleSchema);
