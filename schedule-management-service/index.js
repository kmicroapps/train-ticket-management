require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const redis = require("redis");

const scheduleRoutes = require("./routes/schedule.route");

const app = express();
const PORT = process.env.PORT || 3001;
const MONGODB_URI = process.env.MONGO_URI;

const redisClient = redis.createClient({ url: "redis://host.docker.internal" });

redisClient.on("error", (err) => console.log("Redis client error:", err));
redisClient.connect().catch(console.error);

app.use(bodyParser.json());

app.use((req, res, next) => {
  req.context = {
    redisClient,
  };

  next();
});

app.use("/", scheduleRoutes);

mongoose
  .connect(MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .catch((err) => console.log(err));

app.listen(PORT, () =>
  console.log(`Ticket Management service running on port ${PORT}`)
);
