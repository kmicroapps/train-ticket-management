const Schedule = require('../models/schedule.model');

exports.createSchedule = async (req, res) => {
	const { event, date, venue } = req.body;
	const { redisClient } = req.context;

	try {
		const newSchedule = new Schedule({ event, date, venue });
		const schedule = await newSchedule.save();

		const schedulesFromCache = await redisClient.get('schedules');

		if (schedulesFromCache) {
			const schedules = JSON.parse(schedulesFromCache);
			schedules.push(schedule);
			await redisClient.set('schedules', JSON.stringify(schedules), {
				EX: 3600,
			});
		} else {
			const schedules = await Schedule.find();
			await redisClient.set('schedules', JSON.stringify(schedules), {
				EX: 3600,
			});
		}

		res.status(201).json(schedule);
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

exports.getSchedules = async (req, res) => {
	const { redisClient } = req.context;

	try {
		const data = await redisClient.get('schedules');

		if (data) {
			const schedules = JSON.parse(data);
			return res.status(200).json(schedules);
		} else {
			const schedules = await Schedule.find();
			await redisClient.set('schedules', JSON.stringify(schedules), {
				EX: 3600,
			});
			return res.status(200).json(schedules);
		}
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

exports.getScheduleById = async (req, res) => {
	try {
		const schedule = await Schedule.findById(req.params.id);
		if (!schedule) {
			return res.status(404).json({ message: 'Schedule not found' });
		}
		res.status(200).json(schedule);
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

exports.updateSchedule = async (req, res) => {
	const { redisClient } = req.context;
	try {
		const schedule = await Schedule.findByIdAndUpdate(
			req.params.id,
			req.body,
			{ new: true }
		);
		if (!schedule) {
			return res.status(404).json({ message: 'Schedule not found' });
		}

		const schedulesFromCache = await redisClient.get('schedules');

		if (schedulesFromCache) {
			const schedules = JSON.parse(schedulesFromCache);
			const updatedSchedules = schedules.map((s) =>
				s._id === req.params.id ? schedule : s
			);
			await redisClient.set(
				'schedules',
				JSON.stringify(updatedSchedules),
				{
					EX: 3600,
				}
			);
		}

		res.status(200).json(schedule);
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

exports.deleteSchedule = async (req, res) => {
	const { redisClient } = req.context;
	try {
		const schedule = await Schedule.findByIdAndDelete(req.params.id);
		if (!schedule) {
			return res.status(404).json({ message: 'Schedule not found' });
		}

		const schedulesFromCache = await redisClient.get('schedules');

		if (schedulesFromCache) {
			const schedules = JSON.parse(schedulesFromCache);
			const updatedSchedules = schedules.filter(
				(s) => s._id !== req.params.id
			);
			await redisClient.set(
				'schedules',
				JSON.stringify(updatedSchedules),
				{
					EX: 3600,
				}
			);
		}

		res.status(200).json({ message: 'Schedule deleted' });
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};
