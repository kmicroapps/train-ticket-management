const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PaymentSchema = new Schema({
	reservationId: {
		type: Schema.Types.ObjectId,
		ref: 'Reservation',
		required: true,
	},
	userId: { type: Schema.Types.ObjectId, ref: 'User', required: true },
	amount: { type: Number, required: true },
	paymentDate: { type: Date, default: Date.now },
	status: {
		type: String,
		enum: ['paid', 'pending', 'failed'],
		default: 'pending',
	},
});

module.exports = mongoose.model('Payment', PaymentSchema);
