require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const paymentRoutes = require('./routes/payment.route');

const app = express();
const PORT = process.env.PORT || 3001;
const MONGODB_URI = process.env.MONGO_URI;

app.use(bodyParser.json());
app.use('/', paymentRoutes);

mongoose
	.connect(MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true })
	.then(() =>
		app.listen(PORT, () =>
			console.log(`Ticket Management service running on port ${PORT}`)
		)
	)
	.catch((err) => console.log(err));
