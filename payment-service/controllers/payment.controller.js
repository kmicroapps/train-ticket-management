const Payment = require('../models/payment.model');
const axios = require('axios');

const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

// Create a new payment
exports.createPayment = async (req, res) => {
	const Authorization = req.headers['authorization'];
	const { reservationId, userId } = req.body;

	try {
		const maxRetries = 10;
		let attempts = 0;
		let reservationResponse;

		while (attempts < maxRetries) {
			console.log('retry......');
			try {
				reservationResponse = await axios.get(
					`${process.env.API_GATEWAY_URI}/reservation/${reservationId}`,
					{
						headers: {
							Authorization,
						},
					}
				);
				if (reservationResponse.status === 200) {
					break;
				}
			} catch (err) {
				if (attempts === maxRetries - 1) {
					throw new Error(
						'Failed to fetch reservation details after multiple attempts'
					);
				}
			}
			attempts += 1;
			await delay(3000);
		}

		const reservationData = reservationResponse.data;
		const ticketIds = reservationData.ticketIds;

		let totalAmount = 0;

		for (const ticketId of ticketIds) {
			const ticketResponse = await axios.get(
				`${process.env.API_GATEWAY_URI}/ticket/detail/${ticketId}`
			);

			const ticketData = ticketResponse.data;

			if (!ticketData) {
				return res
					.status(404)
					.json({ message: `Ticket with ID ${ticketId} not found` });
			}
			totalAmount += ticketData.price;
		}

		const newPayment = new Payment({
			reservationId,
			userId,
			amount: totalAmount,
		});

		const payment = await newPayment.save();

		const updateReservationResponse = await axios.patch(
			`${process.env.API_GATEWAY_URI}/reservation/${reservationId}/payment`,
			{
				paymentStatus: 'completed',
			},
			{
				headers: {
					Authorization,
				},
			}
		);

		if (updateReservationResponse.status !== 200) {
			throw new Error('Failed to update reservation status');
		}

		res.status(201).json(payment);
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

// Get all payments
exports.getPayments = async (req, res) => {
	try {
		const payments = await Payment.find()
			.populate('reservationId')
			.populate('userId');
		res.status(200).json(payments);
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

// Get payment by ID
exports.getPaymentById = async (req, res) => {
	try {
		const payment = await Payment.findById(req.params.id)
			.populate('reservationId')
			.populate('userId');
		if (!payment) {
			return res.status(404).json({ message: 'Payment not found' });
		}
		res.status(200).json(payment);
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

// Update a payment
exports.updatePayment = async (req, res) => {
	try {
		const payment = await Payment.findByIdAndUpdate(
			req.params.id,
			req.body,
			{ new: true }
		);
		if (!payment) {
			return res.status(404).json({ message: 'Payment not found' });
		}
		res.status(200).json(payment);
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};

// Delete a payment
exports.deletePayment = async (req, res) => {
	try {
		const payment = await Payment.findByIdAndDelete(req.params.id);
		if (!payment) {
			return res.status(404).json({ message: 'Payment not found' });
		}
		res.status(200).json({ message: 'Payment deleted' });
	} catch (error) {
		res.status(500).json({ error: error.message });
	}
};
