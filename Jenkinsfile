pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                dir('api-gateway') {
                    withCredentials([file(credentialsId: 'gateway_prod_env', variable: 'gateway_prod_env')]) {
                        sh "ls -la"
                        sh "cp /$gateway_prod_env .env.production"
                    }
                    withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                        sh "docker build -t kaitohasei/ttm-api-gateway ."
                        sh "docker push kaitohasei/ttm-api-gateway"
                    }
                }
                dir('auth-service') {
                    withCredentials([file(credentialsId: 'auth_prod_env', variable: 'auth_prod_env')]) {
                        sh "ls -la"
                        sh "cp /$auth_prod_env .env.production"
                    }
                    withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                        sh "docker build -t kaitohasei/ttm-auth-service ."
                        sh "docker push kaitohasei/ttm-auth-service"
                    }
                }
                dir('payment-service') {
                    withCredentials([file(credentialsId: 'payment_prod_env', variable: 'payment_prod_env')]) {
                        sh "ls -la"
                        sh "cp /$payment_prod_env .env"
                    }
                    withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                        sh "docker build -t kaitohasei/ttm-payment-service ."
                        sh "docker push kaitohasei/ttm-payment-service"
                    }
                }
                dir('schedule-management-service') {
                    withCredentials([file(credentialsId: 'schedule_prod_env', variable: 'schedule_prod_env')]) {
                        sh "ls -la"
                        sh "cp /$schedule_prod_env .env"
                    }
                    withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                        sh "docker build -t kaitohasei/ttm-schedule-service ."
                        sh "docker push kaitohasei/ttm-schedule-service"
                    }
                }
                dir('ticket-management-service') {
                    withCredentials([file(credentialsId: 'ticket_prod_env', variable: 'ticket_prod_env')]) {
                        sh "ls -la"
                        sh "cp /$ticket_prod_env .env"
                    }
                    withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                        sh "docker build -t kaitohasei/ttm-ticket-service ."
                        sh "docker push kaitohasei/ttm-ticket-service"
                    }
                }
                dir('ticket-reservation-service') {
                    withCredentials([file(credentialsId: 'revert-ticket_prod_env', variable: 'revert_ticket_prod_env')]) {
                        sh "ls -la"
                        sh "cp /$revert_ticket_prod_env .env"
                    }
                    withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                        sh "docker build -t kaitohasei/ttm-revert-ticket-service ."
                        sh "docker push kaitohasei/ttm-revert-ticket-service"
                    }
                }
            }
        }

        // stage('Deploy to Dev') {
        //     steps {
        //         echo "Deploying and cleaning api gateway"
        //         sh "docker pull kaitohasei/ttm-api-gateway"
        //         sh "docker container stop ttm-api-gateway || echo 'This container does not exist!'"
        //         sh "echo y | docker container prune"

        //         sh "docker container run -d --name ttm-api-gateway -p 8081:8080 kaitohasei/ttm-api-gateway"
                
        //         echo "Deploying and cleaning auth service"
        //         sh "docker pull kaitohasei/ttm-auth-service"
        //         sh "docker container stop ttm-auth-service || echo 'This container does not exist!'"
        //         sh "echo y | docker container prune"

        //         sh "docker container run -d --name ttm-auth-service -p 8082:8081 kaitohasei/ttm-auth-service"
        //     }
        // }

        stage('Deploy to production') {
            agent {
                docker {
                    image 'khaliddinh/ansible'
                }
            }

            environment {
                ANSIBLE_HOST_KEY_CHECKING = 'False'
            }

            steps {
                dir('api-gateway') {
                    withCredentials([file(credentialsId: 'hosts_gateway', variable: 'hosts_gateway'), file(credentialsId: 'playbook_gateway', variable: 'playbook_gateway')]) {
                        sh "ls -la"
                        sh "cp /$hosts_gateway hosts"
                        sh "cp /$playbook_gateway playbook.yml"
                    }

                    withCredentials([file(credentialsId: 'ansible_key', variable: 'ansible_key')]) {
                        sh 'ls -la'
                        sh "cp /$ansible_key ansible_key"
                        sh 'cat ansible_key'
                        sh 'ansible --version'
                        sh 'ls -la'
                        sh 'chmod 400 ansible_key '
                        sh 'ansible-playbook -i hosts --private-key ansible_key playbook.yml'
                    }
                }

                dir('auth-service') {
                    withCredentials([file(credentialsId: 'hosts_auth', variable: 'hosts_auth'), file(credentialsId: 'playbook_auth', variable: 'playbook_auth')]) {
                        sh "ls -la"
                        sh "cp /$hosts_auth hosts"
                        sh "cp /$playbook_auth playbook.yml"
                    }

                    withCredentials([file(credentialsId: 'ansible_key', variable: 'ansible_key')]) {
                        sh 'ls -la'
                        sh "cp /$ansible_key ansible_key"
                        sh 'cat ansible_key'
                        sh 'ansible --version'
                        sh 'ls -la'
                        sh 'chmod 400 ansible_key '
                        sh 'ansible-playbook -i hosts --private-key ansible_key playbook.yml'
                    }
                }

                dir('payment-service') {
                    withCredentials([file(credentialsId: 'hosts_payment', variable: 'hosts_payment'), file(credentialsId: 'playbook_payment', variable: 'playbook_payment')]) {
                        sh "ls -la"
                        sh "cp /$hosts_payment hosts"
                        sh "cp /$playbook_payment playbook.yml"
                    }

                    withCredentials([file(credentialsId: 'ansible_key', variable: 'ansible_key')]) {
                        sh 'ls -la'
                        sh "cp /$ansible_key ansible_key"
                        sh 'cat ansible_key'
                        sh 'ansible --version'
                        sh 'ls -la'
                        sh 'chmod 400 ansible_key '
                        sh 'ansible-playbook -i hosts --private-key ansible_key playbook.yml'
                    }
                }

                dir('schedule-management-service') {
                    withCredentials([file(credentialsId: 'hosts_schedule', variable: 'hosts_schedule'), file(credentialsId: 'playbook_schedule', variable: 'playbook_schedule')]) {
                        sh "ls -la"
                        sh "cp /$hosts_schedule hosts"
                        sh "cp /$playbook_schedule playbook.yml"
                    }

                    withCredentials([file(credentialsId: 'ansible_key', variable: 'ansible_key')]) {
                        sh 'ls -la'
                        sh "cp /$ansible_key ansible_key"
                        sh 'cat ansible_key'
                        sh 'ansible --version'
                        sh 'ls -la'
                        sh 'chmod 400 ansible_key '
                        sh 'ansible-playbook -i hosts --private-key ansible_key playbook.yml'
                    }
                }

                dir('ticket-management-service') {
                    withCredentials([file(credentialsId: 'hosts_ticket', variable: 'hosts_ticket'), file(credentialsId: 'playbook_ticket', variable: 'playbook_ticket')]) {
                        sh "ls -la"
                        sh "cp /$hosts_ticket hosts"
                        sh "cp /$playbook_ticket playbook.yml"
                    }

                    withCredentials([file(credentialsId: 'ansible_key', variable: 'ansible_key')]) {
                        sh 'ls -la'
                        sh "cp /$ansible_key ansible_key"
                        sh 'cat ansible_key'
                        sh 'ansible --version'
                        sh 'ls -la'
                        sh 'chmod 400 ansible_key '
                        sh 'ansible-playbook -i hosts --private-key ansible_key playbook.yml'
                    }
                }

                dir('ticket-reservation-service') {
                    withCredentials([file(credentialsId: 'hosts_ticket-revert', variable: 'hosts_ticket_revert'), file(credentialsId: 'playbook_ticket-revert', variable: 'playbook_ticket_revert')]) {
                        sh "ls -la"
                        sh "cp /$hosts_ticket_revert hosts"
                        sh "cp /$playbook_ticket_revert playbook.yml"
                    }

                    withCredentials([file(credentialsId: 'ansible_key', variable: 'ansible_key')]) {
                        sh 'ls -la'
                        sh "cp /$ansible_key ansible_key"
                        sh 'cat ansible_key'
                        sh 'ansible --version'
                        sh 'ls -la'
                        sh 'chmod 400 ansible_key '
                        sh 'ansible-playbook -i hosts --private-key ansible_key playbook.yml'
                    }
                }
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}