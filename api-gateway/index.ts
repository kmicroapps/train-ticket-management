import express from "express";
import proxy from "express-http-proxy";
import rateLimit from "express-rate-limit";
import bodyParser from "body-parser";
import morgan from "morgan";
import cors from "cors";

import config from "./configs/environment";
import { errorHandler } from "./middlewares/errorHandler";
import { authenticationMiddleware } from "./middlewares/authentication.middleware";

const app = express();
const limiter = rateLimit({
  windowMs: 60 * 1000, // 10 minutes,
  max: 10,
  handler: (req, res) => {
    res.status(429).json({
      error: {
        message: "Too many requests!",
      },
    });
  },
});

app.use(morgan("dev"));
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(limiter);

app.use("^/$", (req, res) => {
  return res.status(404).end();
});

app.use(`${config.PREFIX}/auth`, proxy(config.AUTH_URL));
app.use(`${config.PREFIX}/ticket`, proxy(config.TICKET_MANAGEMENT_URL));
app.use(`${config.PREFIX}/reservation`,authenticationMiddleware, proxy(config.TICKET_RESERVATION_URL));
app.use(`${config.PREFIX}/payment`,authenticationMiddleware, proxy(config.PAYMENT_URL));
app.use(`${config.PREFIX}/schedule`, proxy(config.SCHEDULE_URL));

app.use(errorHandler);

app.listen(config.PORT, () =>
  console.log(`server is running on: http://${config.HOST}:${config.PORT}`)
);
