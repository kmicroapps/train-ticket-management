import { NextFunction, Request, Response } from "express";
import axios from 'axios';

import config from "../configs/environment";

import { UnAuthorizeError } from "../types/errors";

const regex = /Bearer (.+)/i;

const authenticationMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const token = req.headers["authorization"]?.match(regex)?.[1] || "";

    if (!token) throw new UnAuthorizeError();
    const response = await axios.get(`${config.AUTH_URL}/verifyToken?token=${token}`)
    
    const verified = await response.data;

    if (!verified) throw new UnAuthorizeError();

    next();
  } catch (error: any) {
    next(error);
  }
};

export { authenticationMiddleware };
