import { ErrorRequestHandler, NextFunction, Request, Response } from "express";

import {
  BadRequestError,
  ForbiddenError,
  UnAuthorizeError,
} from "../types/errors";

export const errorHandler: ErrorRequestHandler = (
  error: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (error instanceof BadRequestError)
    return res
      .status(error.statusCode)
      .json({ error: { message: error.message } });

  if (error instanceof UnAuthorizeError)
    return res
      .status(error.statusCode)
      .json({ error: { message: error.message } });

  if (error instanceof ForbiddenError)
    return res
      .status(error.statusCode)
      .json({ error: { message: error.message } });

  console.log(error);
  return res.status(500).json({ error: { message: "something went wrong" } });
};
