import dotenv from "dotenv";
import path from "path";

dotenv.config({
  path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV}`),
});

export default {
  NODE_ENV: process.env.NODE_ENV || "development",
  HOST: process.env.HOST || "localhost",
  PORT: process.env.PORT || "8080",
  PREFIX: process.env.PREFIX || "",
  AUTH_URL: process.env.AUTH_URL || "",
  TICKET_MANAGEMENT_URL: process.env.TICKET_MANAGEMENT_URL || "http://localhost:8001",
  TICKET_RESERVATION_URL: process.env.TICKET_RESERVATION_URL || "http://localhost:8002",
  PAYMENT_URL: process.env.PAYMENT_URL || "http://localhost:8003",
  SCHEDULE_URL: process.env.SCHEDULE_URL || "http://localhost:8003",
};
