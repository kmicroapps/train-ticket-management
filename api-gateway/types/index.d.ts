import { Context } from "./index";

declare global {
  namespace Express {
    export interface Request {
      context: Context;
    }
  }
}
